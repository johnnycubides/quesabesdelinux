\select@language {spanish}
\contentsline {section}{\numberline {1}Archivos de configuraci\IeC {\'o}n}{2}{section.1}
\contentsline {section}{\numberline {2}Particiones Linux}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}Particiones Device}{2}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}SCSI}{3}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}SAS}{3}{subsubsection.2.1.2}
\contentsline {section}{\numberline {3}Una Introducci\IeC {\'o}n al BASH}{4}{section.3}
\contentsline {subsection}{\numberline {3.1}Control De tareas}{4}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Variables de entorno}{4}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}Impresi\IeC {\'o}n de las variables de entorno}{4}{subsubsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.2}Configuraci\IeC {\'o}n de las variables de entorno}{4}{subsubsection.3.2.2}
\contentsline {subsection}{\numberline {3.3}Pipes}{5}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Redirecci\IeC {\'o}n}{5}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Atajos de la l\IeC {\'\i }nea de comandos BASH}{5}{subsection.3.5}
\contentsline {subsubsection}{\numberline {3.5.1}Expresi\IeC {\'o}n del nombre de archivo}{6}{subsubsection.3.5.1}
\contentsline {subsubsection}{\numberline {3.5.2}Variables de entorno como par\IeC {\'a}metros}{6}{subsubsection.3.5.2}
\contentsline {subsubsection}{\numberline {3.5.3}Uso de varios comandos}{7}{subsubsection.3.5.3}
\contentsline {subsubsection}{\numberline {3.5.4}Comillas invertidas ({\tt \char `\`{}})}{7}{subsubsection.3.5.4}
\contentsline {section}{\numberline {4}HERRAMIENTAS DE DOCUMENTACI\IeC {\'O}N}{7}{section.4}
\contentsline {subsection}{\numberline {4.1}man (abreviatura de manual)}{7}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}info (texinfo)}{8}{subsection.4.2}
